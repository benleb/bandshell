# bandshell-browser.rb
# Usage: bandshell-browser.rb ConcertoURL
# Where ConcertoURL Looks like http://localhost:4567
#
# Periodically ping the bandshell web app so that background tasks
# may be performed. Called by bandshelld as a daemon.
require "net/http"

def linestamp
  "bandshell-browser ("+Time.now.to_s+"): "
end

puts ""
puts linestamp + "Bandshell Browser Startup."
BandshellURL1 = ARGV[0]

if BandshellURL1.nil? or BandshellURL1.empty?
  raise linestamp + "Parameter Bandshell URL is required."
end

puts linestamp + "Ping bandshell at " + BandshellURL1

StatusURI1 = URI.parse(BandshellURL1+"/screen")

loop do
  sleep(5)
  #if a system password is stored as a config, we need to restart bandshelld
  #to execute chpasswd and clear that config
  # if Bandshell::ConfigStore.config_exists?('system_password')
  #   Bandshell::Passwords.set_local_passwords
  # end

  begin
    response = Net::HTTP.get_response(StatusURI1)
  rescue Errno::ECONNREFUSED
    puts linestamp + "Bandshell is not responding."
  rescue SocketError
    puts linestamp + "Could not connect to given URL."
  rescue => e
    puts linestamp + "Unknown error occurred."
    puts e.inspect
    puts e.backtrace
  else
    puts linestamp + "Starting Chromium.."
    @cmd = "chromium-browser' #{StatusURI1}'"
    `#{@cmd}`
    # `su concerto -c 'chromium-browser ' + BandshellURL1+"/screen"`
    break
  end
end
